import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AlertService } from '../services/alert.service';
import { DateService } from '../services/date.services';
import { PaymentsService } from '../services/payments.service';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
})
export class PaymentsComponent implements OnInit {
  
  searchForm: FormGroup;
  loading: boolean = false;

  constructor(
    public paymentsService: PaymentsService,
    private alertService: AlertService,
    public dateService: DateService
  ) {
    this.searchForm = new FormGroup({
      dateFrom: new FormControl(new Date(), Validators.required),
      dateTo: new FormControl(new Date(), Validators.required),
      taxId: new FormControl(null, [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.min(10000000000),
        Validators.max(99000000000),
      ])
    });
  }

  ngOnInit(): void {}

  search() {
    
    if (!this.searchForm.valid) {
      this.alertService.alert('Verifique que todos los datos estén cargados y sean correctos');
      return;
    }

    this.loading = true;
    let filters = this.searchForm.value;

    filters.dateFrom = this.dateService.dateFormat(filters.dateFrom);
    filters.dateTo = this.dateService.dateFormat(filters.dateTo);

    this.paymentsService.downloadPDF(filters).subscribe(
      (resp) => {
        this.loading = false;
        const fileURL = window.URL.createObjectURL(resp);
        const tab = window.open();
        tab.location.href = fileURL;
      },
      (err) => {
        this.loading = false;
        err.then((err2) => {
          err2 = JSON.parse(err2);
          if (err2.status && err2.status == 403) {
            this.alertService.alert(err2.message);
          } else {
            this.alertService.alert(err2.errors[0].message);
          }
        });
      }
    );
  }
}
