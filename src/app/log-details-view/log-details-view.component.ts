import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router, ActivatedRoute } from '@angular/router';
import { LogService } from '../services/log.service';
import { map } from 'rxjs/operators';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-log-details-view',
  templateUrl: './log-details-view.component.html',
  styleUrls: ['./log-details-view.component.scss']
})
export class LogDetailsViewComponent implements OnInit {

  uuid: string;

  displayedColumns: string[] = ['uuid', 'date', 'detailCodeText', 'detail'];
  dataSource: any;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;



  constructor(
    private logService: LogService,
    public router: Router,
    public activatedRoute: ActivatedRoute,
    private alertService: AlertService
  ) { }

  ngOnInit(): void {

    this.dataSource = new MatTableDataSource([])

    this.activatedRoute.params.subscribe(params => {

      this.uuid = params.id;

      this.logService.getLogsDetails(this.uuid).subscribe(
        resp => {
          console.log('resp', resp);
          resp.eventDetailList.map(det => {

            switch (det.detailCode) {
              case 1:
                det.detailCodeText = 'IN'
                break;
              case 2:
                det.detailCodeText = 'OUT'
                break;
              default:
                det.detailCodeText = 'OTHER'
                break;
            }

            return det;
          })
          this.dataSource = new MatTableDataSource(resp.eventDetailList);
        },
        (err) => {
          err.then((err2) => {
            err2 = JSON.parse(err2);
            if (err2.status && err2.status == 403) {
              this.alertService.alert(err2.message);
            } else {
              this.alertService.alert(err2.errors[0].message);
            }
          });
        }
      )
    });



  }

}
