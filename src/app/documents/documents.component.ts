import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AlertService } from '../services/alert.service';
import { DateService } from '../services/date.services';
import { DocumentsService } from '../services/documents.service';
import {MatTabsModule} from '@angular/material/tabs';

@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss'],
})
export class DocumentsComponent implements OnInit {
  searchForm: FormGroup;
  documentTypes: any[];
  loading: boolean = false;

  constructor(
    public documentsService: DocumentsService,
    private alertService: AlertService,
    public dateService: DateService
  ) {
    const fecha = new Date(new Date().getFullYear(),new Date().getMonth() , new Date().getDate());
    this.searchForm = new FormGroup({
      dateFrom: new FormControl(fecha, Validators.required),
      dateTo: new FormControl(fecha, Validators.required),
      taxId: new FormControl(null, [
        Validators.pattern('^[0-9]*$'),
        Validators.min(10000000000),
        Validators.max(99000000000),
      ]),
      documentType: new FormControl(),
      pos: new FormControl(null, [
        Validators.pattern('^[0-9]*$'),
        Validators.min(1),
        Validators.max(9999),
      ]),
      documentNumberFrom: new FormControl(),
      documentNumberTo: new FormControl(),
    });
  }

  ngOnInit(): void {
    this.getDocumentTypes();
  }

  search() {

    if (!this.searchForm.valid) {
      this.alertService.alert('Verifique los datos cargados');
      return;
    }

    this.loading = true;
    let filters = this.searchForm.value;

    filters.dateFrom = this.dateService.dateFormat(filters.dateFrom);
    filters.dateTo = this.dateService.dateFormat(filters.dateTo);

    this.documentsService.downloadPDF(filters).subscribe(
      (resp) => {
        this.loading = false;
        const fileURL = window.URL.createObjectURL(resp);
        const tab = window.open();
        tab.location.href = fileURL;
      },
      (err) => {
        this.loading = false;
        err.then((err2) => {
          err2 = JSON.parse(err2);
          if (err2.status && err2.status == 403) {
            this.alertService.alert(err2.message);
          } else {
            this.alertService.alert(err2.errors[0].message);
          }
        });
      }
    );
  }

  getDocumentTypes() {
    this.documentsService.getDocumentTypes().subscribe((res) => {
      this.documentTypes = res;
    });
  }


}
