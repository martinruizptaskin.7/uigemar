export interface LogData {
  uuid: string;
  date: Date;
  endPoint: string;
  method: string;
  statusCode: number;
  statusMessage: string;
}
