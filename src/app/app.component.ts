import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  
  isLogedIn = false;
  menu = {
    viewConsole: false,
    viewDocuments: false,
    viewPayments: false,
  }

  constructor(
    public authService: AuthService,
    public router: Router,
  ) {
    this.authService.isLogedIn.subscribe( res => { 
      this.isLogedIn = res 
      this.getMenuAccess();
    })
    this.redirect();
  }

  redirect() {
    if (!this.isLogedIn) {
      this.logout();
    }
  }

  getMenuAccess() {
    this.menu.viewConsole = this.authService.hasRole('ROLE_ADMINCONSOLE');
    this.menu.viewDocuments = this.authService.hasRole('ROLE_REPRINT');
    this.menu.viewPayments = this.authService.hasRole('ROLE_GEMARPAYMENT');
  }

  logout(){
    this.authService.logout();
  }

}
