import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MinAmountComponent } from './min-amount.component';

describe('MinAmountComponent', () => {
  let component: MinAmountComponent;
  let fixture: ComponentFixture<MinAmountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MinAmountComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MinAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
