import { Component, OnInit } from '@angular/core';
import { MinAmountService } from '../services/min-amount.service';
import { AuthService } from '../services/auth.service';
@Component({
  selector: 'app-min-amount',
  templateUrl: './min-amount.component.html',
  styleUrls: ['./min-amount.component.scss']
})
export class MinAmountComponent implements OnInit {

  constructor(private minamountservice:MinAmountService, private authService:AuthService) { }
public IibbPerception:any
public newvalue:Number
public IibbRetencion
public errorcode:string=""
public display :string="none"
public success:boolean
public error :boolean
public selected:any={
  TaxName:"",
  TaxID:"",
  minimumTax:""
}
  ngOnInit(): void {
    this.getdata()

  }
  getdata(){
    this.minamountservice.getDocumentTypes().subscribe(res=>{
      console.log(res)
      res.map(x=>{if(x.taxId==2){
          this.IibbPerception=x
          console.log(this.IibbPerception)
      }
    else if(x.taxId==4){
      this.IibbRetencion=x
      console.log(this.IibbPerception)
    }
    }

      )
    })
  }


  changevalue(value){
    this.display="flex"
    this.selected=value

  }
  closeModal(){
    this.display="none"
    this.error=false
    this.success=false
  }
  updateamount(){
    console.log(this.selected)
    let values={
      "code": this.selected.taxId,
    "amount" : +this.newvalue,
    "user":this.authService.usuario

    }
    if(this.newvalue==undefined){
      this.errorcode="ingrese un nuevo monto para "+ this.selected.TaxName
      alert(this.errorcode)
    }
    else{
      let response=this.minamountservice.updateminamount(values).subscribe(res=>{
        console.log("hols")
      console.log(  res.error )
      if(res.mensaje==null){
        this.getdata()
        this.success=true

      }
      else{
        this.error=true

      }
    }
    ,err=>{this.error=true; this.errorcode='por favor vuelva a intentar'}
    )}

  }
}
