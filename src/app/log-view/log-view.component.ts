import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { LogService } from '../services/log.service';
import { AlertService } from '../services/alert.service';

@Component({
  selector: 'app-log-view',
  templateUrl: './log-view.component.html',
  styleUrls: ['./log-view.component.scss'],
})
export class LogViewComponent implements OnInit {
  displayedColumns: string[] = [
    'date',
    'endPoint',
    'method',
    'statusCode',
    'statusMessage',
    'actions',
  ];
  dataSource: any;

  dateFrom = new FormControl(new Date());
  dateTo = new FormControl(new Date());
  endPoint = new FormControl();
  method = new FormControl();
  status = new FormControl();
  statusCode = new FormControl();
  statusMessage = new FormControl();
  detail =new FormControl();

  primerFiltro = true;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    private logService: LogService,
    public router: Router,
    private alertService: AlertService) {
    this.dateFrom.patchValue(logService.filters.dateFrom);
    this.dateTo.patchValue(logService.filters.dateTo);
    this.endPoint.patchValue(logService.filters.endPoint);
    this.method.patchValue(logService.filters.method);
    this.statusCode.patchValue(logService.filters.statusCode);
    this.statusMessage.patchValue(logService.filters.statusMessage);
  }

   ngOnInit(): void {

    this.dataSource = new MatTableDataSource([]);

    this.logService.getLogs().subscribe(
      (resp) => {

        this.dataSource = new MatTableDataSource(resp.eventList);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;

        this.dataSource.filterPredicate = (data: any, filtersJson: string) => {
          const matchFilter = [];
          const filters = JSON.parse(filtersJson);



          return matchFilter.every(Boolean);
        };

        this.applyFilter();
      },
      (err) => {
        err.then((err2) => {
          err2 = JSON.parse(err2);
          if (err2.status && err2.status == 403) {
            this.alertService.alert(err2.message);
          } else {
            this.alertService.alert(err2.errors[0].message);
          }
        });
      });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.paginator.page.subscribe(
      (event) => {
        this.logService.pageIndex = event.pageIndex;
      });
  }

  applyFilter() {
    const d = new Date();
    const dateFrom = this.dateFrom.value
      ? new Date(
        this.dateFrom.value.getFullYear(),
        this.dateFrom.value.getMonth(),
        this.dateFrom.value.getDate(),
        0,
        0,
        0
      )
      : null;
    const dateTo = this.dateTo.value
      ? new Date(
        this.dateTo.value.getFullYear(),
        this.dateTo.value.getMonth(),
        this.dateTo.value.getDate(),
        23,
        59,
        59
      )
      : null;

    this.logService.filters.endPoint = this.endPoint.value || '';
    this.logService.filters.method = this.method.value || '';
    this.logService.filters.statusCode = this.statusCode.value;
    this.logService.filters.statusMessage = this.statusMessage.value || '';
    this.logService.filters.dateFrom =
      dateFrom || new Date(d.getFullYear() - 30, 1, 1);
    this.logService.filters.dateTo =
      dateTo || new Date(d.getFullYear() + 30, 1, 1);
    const tableFilters = [
      { ...this.logService.filters },
    ];

    this.dataSource.filter = JSON.stringify(tableFilters);

    if (this.dataSource.paginator) {
      if (this.primerFiltro) {
        this.goToPage(this.logService.pageIndex);
        this.primerFiltro = false;
      } else {
        this.dataSource.paginator.firstPage();
      };
    }
  }

  gotoDetails(logSelected) {
    this.logService.logSelected = logSelected;
    this.router.navigate([`log-view/${logSelected.uuid}`]);
  }

  reset() {

    const dateFrom = new Date();
    dateFrom.setMonth(dateFrom.getMonth() - 3);

    this.dateFrom.patchValue(dateFrom);
    this.dateTo.patchValue(new Date());
    this.endPoint.reset();
    this.method.reset();
    this.statusCode.reset();
    this.statusMessage.reset();
  }

  goToPage(pageNumber) {
    (this.paginator.pageIndex = pageNumber), // number of the page you want to jump.
      this.paginator.page.next({
        pageIndex: pageNumber,
        pageSize: this.paginator.pageSize,
        length: this.paginator.length,
      });
  }
}
