import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  hide = true;
  forma: FormGroup;
  loading = false;
  mensaje: string;
  
  constructor(
    public router: Router,
    public authService: AuthService,
    private _snackBar: MatSnackBar
  ) {
   }

  ngOnInit(): void {
    this.forma = new FormGroup({
      username: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });

    this.authService.isLogedIn.subscribe( res => {
      res &&  this.router.navigate(['log-view']); 
    } )
  }

  

  login(f) {
    
    if (this.forma.invalid) {
      console.log('error');
      console.log(this.forma);
      return;
    }
    
    this.loading = true;
    this.mensaje = null;
    
    this.authService.login(this.forma.value)
      .subscribe(
        correcto => {
          this.loading = false;
          if (this.authService.hasRole('ROLE_ADMINCONSOLE')) {
            this.router.navigate( [ '/log-view' ] );
          } else {
            this.router.navigate( [ '/documents' ] );
          }
        },
        error => {
          console.log(error);
          
          this.loading = false;
          if (error instanceof HttpErrorResponse) {
            const validationErrors = error.error;
            if (error.status === 422) {
              Object.keys(validationErrors).forEach(prop => {
                const formControl = this.forma.get(prop);
                if (formControl) {
                  formControl.setErrors({
                    serverError: validationErrors[prop]
                  });
                }
              });
            } else {
              this.mensaje = error.message || error.error.message;
              this._snackBar.open(this.mensaje, 'close', {
                duration: 3000,
              });
            }
          } else {
            this.mensaje = error;
          }

        }
      );
  }

}
