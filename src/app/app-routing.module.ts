import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { LogViewComponent } from './log-view/log-view.component';
import { LogDetailsViewComponent } from './log-details-view/log-details-view.component';
import { DocumentsComponent } from './documents/documents.component';
import { PaymentsComponent } from './payments/payments.component';
import { MinAmountComponent } from './min-amount/min-amount.component';
import { LoginGuard } from './services/login.guard';

const routes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'log-view/:id', canActivate: [ LoginGuard ],component: LogDetailsViewComponent},
  {path: 'log-view', canActivate: [ LoginGuard ],component: LogViewComponent},
  {path: 'documents', canActivate: [ LoginGuard ],component: DocumentsComponent},
  {path: 'payments', canActivate: [ LoginGuard ],component: PaymentsComponent},
  {path: 'Amountconfig', canActivate: [ LoginGuard ],component: MinAmountComponent},

  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
