import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AngularMaterialModule } from './angular-material.module';
import { FlexLayoutModule } from '@angular/flex-layout';

import { LoginComponent } from './auth/login/login.component';
import { LogViewComponent } from './log-view/log-view.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { LogDetailsViewComponent } from './log-details-view/log-details-view.component';
import { TokenInterceptor } from './services/token.interceptor';
import { MatTableExporterModule } from 'mat-table-exporter';
import { DocumentsComponent } from './documents/documents.component';
import { PaymentsComponent } from './payments/payments.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { MinAmountComponent } from './min-amount/min-amount.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    LogViewComponent,
    LogDetailsViewComponent,
    DocumentsComponent,
    PaymentsComponent,
    MinAmountComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AngularMaterialModule,
    FlexLayoutModule,
    FormsModule,
    ReactiveFormsModule,
    MatTableExporterModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptor, multi: true },
    {provide: MAT_DATE_LOCALE, useValue: 'es-ES'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
