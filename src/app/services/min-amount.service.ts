import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { of, throwError,Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
@Injectable({
  providedIn: 'root'
})
export class MinAmountService {

  constructor(private http:HttpClient) { }
   getDocumentTypes () :Observable <any>  {
    let headers =new HttpHeaders()
    .set("Content-Type","application/json")
    .set("Access-Control_Allow-Origin","*");

    const urlApi =  environment.urlApiServices + '/getTaxes';
    let response =this.http.get(urlApi,{'headers':headers})
    return response
  }

  updateminamount (value) :Observable <any>  {
    let headers =new HttpHeaders()
    .set("Content-Type","application/json")
    .set("Access-Control_Allow-Origin","*");

    const urlApi =  environment.urlApiServices + '/UpdateMinAmuout';
    try{    let response =this.http.post(urlApi,value,{'headers':headers})
    console.log(response)
    return response
  }
  catch(e){
    return e
  }

  }
}
