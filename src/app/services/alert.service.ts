import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private _snackBar: MatSnackBar) { }

  alert(msg: string) {
    const styleClass = new MatSnackBarConfig();
    // styleClass.panelClass = ['mat-toolbar', 'mat-warn'];
    this._snackBar.open(msg, 'ok', styleClass);
  }
}
