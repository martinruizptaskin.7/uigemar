import { TestBed } from '@angular/core/testing';

import { MinAmountService } from './min-amount.service';

describe('MinAmountService', () => {
  let service: MinAmountService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(MinAmountService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
