import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PaymentsService {

  constructor(public http: HttpClient) { }

  downloadPDF(params: any){
    const urlApi = environment.urlReposts + '/getPaymentReport';
    return this.http.post(urlApi, params, { responseType: 'blob' })
    .pipe(
      map((res) => {
        return res
      }),
      catchError( err => {
        return throwError(err.error.text());
      })
    );;
  }
  
}
