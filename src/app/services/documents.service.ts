import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class DocumentsService {
  constructor(public http: HttpClient) {}

  getDocumentTypes() {
    const urlApi = environment.urlApiServices + '/getDocumentTypes';
    return this.http.get(urlApi).pipe(
      map((resp: any) => {
        resp.map(
          item => {
            item.descriptionToUse = item.value + ' ' + item.description
            item.valueToUse = item.description;
          }
        )
        return resp;
      })
    );
  }


  downloadPDF(params: any): any {
    const urlApi = environment.urlReposts + '/reprint_documents';
    return this.http.post(urlApi, params, { responseType: 'blob' })
    .pipe(
      map((res) => {
        return res
      }),
      catchError( err => {
        return throwError(err.error.text());
      })
    );
  }
}
