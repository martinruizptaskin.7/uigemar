import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {
  
  isLogedIn: Boolean = false;

  constructor(
    public authService: AuthService,
    public router: Router
  ) {
    this.authService.isLogedIn.subscribe(
      resp => {
        this.isLogedIn = resp;
      }

    )
  }

  canActivate() {
console.log('this.isLogedIn', this.isLogedIn);

    if ( this.isLogedIn ) {
      return true;
    } else {
      console.log( 'Bloqueado por LoginGuard' );
      this.router.navigate(['/login']);
      return false;
    }

  }

}
