import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { BehaviorSubject, throwError } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  usuario: any;
  token = '';
  roles = [];

  public isLogedIn: BehaviorSubject<boolean>;

  constructor(public http: HttpClient, public router: Router) {
    this.isLogedIn = new BehaviorSubject(false);
    this.leerStorage();
  }

  login(usuario: any, recordar: boolean = false) {
    let headers =new HttpHeaders()
    .set("Content-Type","application/json")
    .set("Access-Control_Allow-Origin","*");
    const url = environment.urlApiServices + '/login';

    return this.http.post(url, usuario,{'headers':headers}).pipe(
      map((resp: any) => {
        this.guardarStorage(resp.access_token, resp.username, resp.roles);
        this.isLogedIn.next(true);
        return true;
      }),
      catchError((err) => {
        console.log('error', err);
        return throwError(err);
      })
    );

  }

  guardarStorage(token: string, usuario: any, roles = []) {
    sessionStorage.setItem('token', token);
    sessionStorage.setItem('roles', JSON.stringify(roles));
    sessionStorage.setItem('usuario', usuario);

    this.usuario = usuario;
    this.token = token;
    this.roles = roles;
  }

  leerStorage() {

    this.token = sessionStorage.getItem('token');
    const usuario = sessionStorage.getItem('usuario');
    this.usuario = usuario ? usuario : null;
    const roles = sessionStorage.getItem('roles');
    this.roles = roles ? JSON.parse(roles) : null;

    if ( this.token && this.token.length > 10 ) {
      this.isLogedIn.next(true);
    }

  }

  logout() {
    const recargar = (this.token && this.token.length) ? true : false;
    this.limpiar();
    recargar && location.reload();
    this.router.navigate(['/login']);
  }

  limpiar() {
    this.usuario = null;
    this.token = '';
    sessionStorage.clear();
    this.isLogedIn.next(false);
  }

  hasRole(roleName: string) {
    return this.roles && this.roles.includes(roleName);
  }

}
