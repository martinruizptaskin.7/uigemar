import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class DateService {

  constructor() {}
  
  dateFormat(date: Date): string {
      const month = ( date.getMonth() + 1) < 10 ? '0' + (date.getMonth() + 1) : (date.getMonth() + 1).toString();
      const newDate = date.getFullYear().toString() +'-'+ month +'-'+ date.getDate().toString();
    return newDate
  }
}