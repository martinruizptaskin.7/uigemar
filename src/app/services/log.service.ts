import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';
import { environment } from 'src/environments/environment';
import { LogData } from '../interfaces/log-date.interface';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class LogService {
  public logSelected: LogData;
  public filters: any;
  public pageIndex = 0;

  constructor(public http: HttpClient, public router: Router) {

    const dateFrom = new Date();
    
    // dateFrom.setMonth(dateFrom.getMonth() - 3);
    
    this.filters = {
      dateFrom: dateFrom,
      dateTo: new Date(),
      endPoint: '',
      method: '',
      statusCode: '',
      statusMessage: ''
    };
  }

  getLogs() {
    const urlApi = environment.urlApiServices + '/interface_events?max=10000&sort=id&order=desc';
    return this.http.get(urlApi).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }

  getLogsDetails(id: any) {
    const urlApi =
      environment.urlApiServices +
      '/interface_event_details' +
      '?max=1000&uuid=' +
      id;
    return this.http.get(urlApi).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }
}
