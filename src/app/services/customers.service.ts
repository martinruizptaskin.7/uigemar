import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

  constructor(public http: HttpClient) { }

  getCustomers(){
    const urlApi = environment.urlApiServices + '/getCustomers';
    return this.http.get(urlApi).pipe(
      map((resp: any) => {
        return resp;
      })
    );
  }
  
}
