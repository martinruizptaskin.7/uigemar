import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { Injectable } from '@angular/core';
import { catchError, mapTo, tap } from 'rxjs/operators';
import { throwError, fromEvent, timer, interval, of } from 'rxjs';
import { AuthService } from './auth.service';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

  private recibiendo;

  constructor(
    public usuarioService: AuthService,
  ) {

  }

  intercept( request: HttpRequest<any>, next: HttpHandler ): Observable<HttpEvent<any>> {

    const token = sessionStorage.getItem( 'token' );

    if ( token ) {
      request = request.clone( {
        setHeaders: {
          Authorization: 'Bearer ' + token
        }
      } );
    }

    let tiempoEspera = 1;

    const source = interval( 1000 );
    const tiempoObservable = source.subscribe( ( val ) => {
      tiempoEspera += 1;
      if ( navigator.onLine ) {
        tiempoEspera === 3 && this.mostrarMsgDemora( 'Estamos teniendo demoras en la respuesta del servidor ' + ( this.recibiendo ? '[Recibiendo]' : '[Sin respuetas]' ) );
        tiempoEspera === 9 && this.mostrarMsgDemora( 'La demora es más alta de la esperada ' + ( this.recibiendo ? '[Recibiendo]' : '[Sin respuetas]' ) );
        tiempoEspera === 15 && this.mostrarMsgDemora( 'Por favor aguarde ' + ( this.recibiendo ? '[Recibiendo]' : '[Sin respuetas]' ) );
      }
    } );

    this.recibiendo = false;
    return next.handle( request ).pipe(
      tap( resp => {
        this.recibiendo = true;
        return of( resp );
      }
        ,
        err => {

          if ( !navigator.onLine ) {
            // si no hay conexión
            this.mostrarMsgError( 'Ups, parace que no hay conexión' );
            tiempoObservable.unsubscribe();
            return throwError( err );
          }

          if ( err.error instanceof ErrorEvent ) {
            // client-side error
            console.log( 'TokenInterceptor client-side error', err.message );
            this.mostrarMsgError( err.message );
          } else {
            // server-side error
            console.log( 'TokenInterceptor server-side error', err.message );
            err.status === 401 && this.usuarioService.logout();
            ( err.status >= 500 || err.status === 404 ) && this.mostrarMsgError( err.message );

          }
          tiempoObservable.unsubscribe();
          return throwError( err );

        },
        () => {
          tiempoObservable.unsubscribe();
        } )
    );
  }

  mostrarMsgError( msg: string ) {
    
  }

  mostrarMsgDemora( msg: string ) {


  }
}
